const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripción de la tarea por hacer'
}

const completado = {
    alias: 'c',
    default: null,
    desc: 'Marca como completado o pendiente la tarea'
}

const argv = require('yargs')
    .command('crear', 'generar actividades', {
        descripcion
    })
    .command('actualizar', 'actualizar actividades', {
        descripcion,
        completado
    })
    .command('listar', 'lista todas las actividades', {
        completado
    })
    .command('borrar', 'borrar actividades', {
        descripcion
    })
    .help()
    .argv;

module.exports = {
    argv
}