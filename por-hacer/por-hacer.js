const fs = require('fs');

let listadoPorHacer = [];

const guardarDB = () => {
    let data = JSON.stringify(listadoPorHacer);

    fs.writeFile('db/data.json', data, (err) => {
        if(err) {
            throw new Error('ERROR: al grabar data');
        }
    });
}

const cargarDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');   
    } catch (error) {
        listadoPorHacer = [];         
    }
}

const getListado = (completado = null) => {
    cargarDB();
    if(completado != null) {
        let nuevoListado = listadoPorHacer.filter( tarea => {
            return tarea.completado !== completado;
        });
    
        return nuevoListado;
    } else {
        return listadoPorHacer;
    }    
}

const actualizar = (descripcion, completado = true) => {
    cargarDB();
    
    let index = listadoPorHacer.findIndex( tarea => {
        return tarea.descripcion === descripcion;
    });

    if (index >= 0) {
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }
}

const crear = (descripcion) => {

    cargarDB();

    let porHacer = {
        descripcion,
        completado: "false"
    }
    listadoPorHacer.push(porHacer);

    guardarDB();
    return porHacer;
}

const borrar = (descripcion) => {
    cargarDB();

    let nuevoListado = listadoPorHacer.filter( tarea => {
        return tarea.descripcion !== descripcion;
    });

    if(nuevoListado.length === listadoPorHacer.length) {        
        return false;
    } else {
        listadoPorHacer = nuevoListado;
        guardarDB();
        return true;
    }
}

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
}